set_xmakever("2.5.2") -- Zig toolchain support

add_rules("mode.debug", "mode.release")

target("math_rust")
    set_kind("static")
    add_files("math_ffi/rust/src/lib.rs")
    after_build(function (target)
        local prefix
        if is_plat("windows") then prefix = ".lib"
        else prefix=".a"
        end
        os.mv(target:targetfile(), path.join(path.directory(target:targetfile()), path.basename(target:targetfile()) .. prefix))
    end)
    set_toolchains("rust")
    
target("math_zig")
    set_kind("static")
    add_files("math_ffi/zig/src/main.zig")
    add_zcflags("-fPIC", "--single-threaded", "--strip", {force = true})
    set_toolchains("zig")

    
target("rust_app")
    set_kind("binary")
    add_files("app_rs/src/*.rs")
    add_deps("math_zig")
    add_linkdirs("$(buildir)")
    set_toolchains("rust")

target("zig_app")
    set_kind("binary")
    add_files("app_zig/src/main.zig")
    add_deps("math_rust")
    set_toolchains("zig")
