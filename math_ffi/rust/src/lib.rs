use std::ffi::CStr;
use std::ffi::CString;
#[repr(C)]
pub struct Doggo {
    age: i32,
    name: CString,
}

impl Doggo {
    fn new() -> Doggo {
        Doggo {
            age: 1,
            name: CString::new("Meu nome é Doggo!").unwrap(),
        }
    }
    fn callName(&mut self, name: &CStr) {
        self.name = CString::new(name.to_str().unwrap()).unwrap();
    }
}

#[no_mangle]
pub extern "C" fn call_name(ptr: *mut Doggo, name: &str) {
    let call = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };
    //call.callName(name);
}

#[no_mangle]
pub extern "C" fn is_whitespace(byte: u8) -> bool {
    match byte {
        b' ' | b'\x09'..=b'\x0d' => true,
        _ => false,
    }
}

#[no_mangle]
pub extern "C" fn mul(value1: usize, value2: usize) -> usize {
    value1 * value2
}

#[no_mangle]
pub extern "C" fn add(value1: usize, value2: usize) -> usize {
    value1 + value2
}
