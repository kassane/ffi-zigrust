const std = @import("std");

pub extern fn mul(value1: usize, value2: usize) usize;
pub extern fn is_whitespace(c: u8) bool;
pub extern fn call_name(ptr: [*c]Doggo, name: [*c]const u8) void;

pub fn main() anyerror!void {
    std.debug.print("Multiply of 5*4={}", .{5*4});
    const chars = [_]u8{ 'a', ' ', 'A', 0x09, 0x0A, 0x0D };

    for (chars) |char, idx| {
        std.debug.warn("{}: is '{c}' whitespace?: {}\n", .{ idx, char, is_whitespace(char) });
    }
    var dg = Doggo{.age=11, .name="Brutus"};
    const d : ?*Doggo = &dg;
    if (d) |doggo| {
        std.debug.print("what your name: {s}\n", .{doggo.name});
        std.debug.print("At what age: {}\n", .{doggo.age});
    } else {
        std.debug.print("it's null\n", .{});
    }
}

pub const Doggo = extern struct {
    age: i32,
    name: [*c]const u8,
};
